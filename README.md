# appriss_rose_install

Welcome to the installation script for our Lambda application PAM replacement.


Troubleshooting:

We have an install script but if it fails there isn't a good way to restart it, and I suspect there will at least be AWS permission issues the first time through. Thus, I recommend following the directions as follows to manually install the dependencies and deploy the code, using the install script only as a guideline. Here you can see the general steps to manually install the app and be ready to push it:

1. Install the AWS command line utility. Instruction to do so can be found here for Linux: https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2-linux.html. Other OS install instructions can be found linked from that page.

2. Install the SAM command line utility. Instruction to do so can be found here for Linux: https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install-linux.html. Other OS install instructions can be found linked from that page.

3. [Optional] Install Docker, as seen here: https://docs.docker.com/engine/install/ubuntu/. Other OS install instructions can be found linked from that page. You only need to do this if you are going to want to run things locally for testing.

4. Log in to the AWS command line utility by running `aws configure` and following the prompts.

5. Save the dependencies of the app to a specific folder by running `python3 -m pip install -r src/requirements.txt -t dependency-layer/python --system`

6. Zip the dependency folder by running `zip -r dependency-layer.zip python` (install zip if needed via `sudo apt install zip`

7. Run `aws lambda publish-layer-version --layer-name appriss_lambda --zip-file fileb://dependency-layer.zip --compatible-runtimes python3.6 python3.7 python3.8 | jq '.LayerVersionArn'` to publish your dependency layer. Note the LayerVersionArn value returned.

8. Delete old layers from the config, making sure to replace `<Function Name>` with the name of the function:`aws lambda update-function-configuration --function-name <Function Name> --layers []`. Note that you should skip this step if you have not deployed the function before.

9. Update the function configuration with your layer version ARN from earlier: `aws lambda update-function-configuration --function-name <Function Name> --layers "<LayerVersionArn>"`

10. Then package your deployment by running, replacing the path to the template.yaml file as needed for your system: `sam package --template-file /home/ubuntu/apprissaws/template.yaml --s3-bucket test-deploy-lambda-pycharm --output-template-file packaged.yaml`

6. Finally deploy the code using the following, replacing the path to the template file and S3 bucket name as needed. `sam deploy --template-file /home/ubuntu/packaged.yaml --stack-name <INSERT S3 BUCKET NAME>`


If you decide to try the install script, instructions are as follows:
1.  Ensure you have access to all of the needed prerequisites. This includes
    an Ubuntu system (script was tested on a clean install of Ubuntu 18.04 on
    an EC2 instance). You also need an IAM role set up for the AWS account you
    will be using that has all the needed permissions (Lambda, API Gateway and
    S3 access at least, potentially more). You also need an s3 bucket set up.
2. Then, run `./install.sh`
3. This sets your system up to deploy. Follow the prompts in the script. After
    the first few prompts you will likely have to wait a while.
4. Then run `source /home/ubuntu/.bashrc`
5. Then run `sam package --template-file /home/ubuntu/apprissaws/template.yaml --s3-bucket test-deploy-lambda-pycharm --output-template-file packaged.yaml`
6. Then run `sam deploy --template-file /home/ubuntu/packaged.yaml --stack-name <INSERT S3 BUCKET NAME>`

