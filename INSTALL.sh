#!/bin/bash

# Created by Justin Calareso Spring 2020
# Assumes you are running Ubuntu (tested on 18.04) on an x86 instruction set (ie most computers other than ARM)
# This should be run in the directory the files should be downloaded into
# Here are some constants we will use

CODE_URL=https://gitlab.com/calaresoj/apprissaws.git
echo "You will likely have to log in to gitlab to download the code. Just follow the prompts."

git clone $CODE_URL
echo ""
echo ""
echo "To successfully deploy the lambda function, you need to have an AWS account with and IAM user with proper permissions."
echo "See https://serverless.com/framework/docs/providers/aws/guide/credentials/ for more info"

echo "Once this is done, please enter your api key and secret below"

read -p 'API Key: ' key
read -sp 'Secret: ' secret



# In case we have any old node_modules installed here, this can be deleted if we remove node_modules from the git repo
rm -rf node_modules

rm -rf src/node_modules
# Now we need to download dependencies, then the code and cd into the directory
sudo apt update
sudo apt -y install build-essential
sudo apt -y install python-setuptools
sudo apt -y install python3-pip
sudo apt -y install unzip
sudo apt -y install zip
sudo apt -y install jq
sudo apt -y install linuxbrew-wrapper

# Installing AWS command line utility

curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install

# And here we install the AWS SAM CLI utility

brew tap aws/tap
brew install aws-sam-cli


# Now we are going to install the SAM command line interface to help integrating with AWS Lambda
# The steps are shown here: https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install-linux.html
# First, we install docker

#sudo apt-get remove docker docker-engine docker.io containerd runc
#sudo apt-get update
#sudo apt-get -y install \
#    apt-transport-https \
#    ca-certificates \
#    curl \
#    gnupg-agent \
#    software-properties-common
#curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
#sudo add-apt-repository \
#   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
#   $(lsb_release -cs) \
#   stable"

#sudo apt-get update
#sudo apt-get -y install docker-ce docker-ce-cli containerd.io

# Now we are adding Homebrew, yes automatically answers yes to everything

#yes | bash -c "$(curl -L https://git.io/n-install)"
#

aws configure set aws_access_key_id "$key"
aws configure set aws_secret_access_key "$secret"

cd apprissaws

mkdir -p dependency-layer/python

python3 -m pip install -r src/requirements.txt -t dependency-layer/python --system

cd dependency-layer
aws lambda publish-layer-version --layer-name appriss_lambda --zip-file fileb://home/ubuntu/apprissaws/dependency-layer/dependency-layer.zip --compatible-runtimes python3.6 python3.7 python3.8 | jq '.LayerARN'
zip -r dependency-layer.zip python

LayerVersionArnQuote=$(aws lambda publish-layer-version --layer-name appriss_lambda --zip-file fileb://dependency-layer.zip --compatible-runtimes python3.6 python3.7 python3.8 | jq '.LayerVersionArn')
temp="${LayerVersionArnQuote%\"}"
LayerVersionArn="${temp#\"}"
echo $LayerVersionArn

#Delete old layers from the config
aws lambda update-function-configuration --function-name TestStack3-HelloWorldFunction-6OQRTGH9673O --layers []


aws lambda update-function-configuration --function-name TestStack3-HelloWorldFunction-6OQRTGH9673O --layers "$LayerVersionArn"

# source /home/ubuntu/.bashrc






